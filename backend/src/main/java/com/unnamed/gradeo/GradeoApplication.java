package com.unnamed.gradeo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradeoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GradeoApplication.class, args);
	}

}
