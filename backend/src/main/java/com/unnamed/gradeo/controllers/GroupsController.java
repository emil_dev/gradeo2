package com.unnamed.gradeo.controllers;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class GroupsController {

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

}
